import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - front-dakota-del-norte-rox',
    title: 'front-dakota-del-norte-rox',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next'
  ],

  router: {
    middleware: ['auth']
  },

  // Axios module 'onfiguration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'http://localhost:8089',
    proxyHeaders: false,
    credentials: false
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: '#607d8b',
          accent: '#eceff1',
          secondary: '#37474f',
          terciary: '#455a64',
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  auth: {
    strategies: {
      local: {
        scheme: 'local',
        endpoints: {
          login: {
            url: '/api/auth/login?provider=local',
            method: 'post',
            propertyName: 'data.token'
          },
          user: false
        },
        token: {
          property: 'data.token',
          global: true
        }
      }
    }
  },

  redirect: {
    login: '/',
    home: '/'
  },

  cookie: {
    prefix: 'auth.',
    options: {
      path: '/login',
      expires: 1
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
